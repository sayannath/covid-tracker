// To parse this JSON data, do
//
//     final data = dataFromJson(jsonString);

import 'dart:convert';

List<Data> dataFromJson(String str) => List<Data>.from(json.decode(str).map((x) => Data.fromJson(x)));

String dataToJson(List<Data> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Data {
  Data({
    this.country,
    this.countryAbbreviation,
    this.totalCases,
    this.newCases,
    this.totalDeaths,
    this.newDeaths,
    this.totalRecovered,
    this.activeCases,
    this.seriousCritical,
    this.casesPerMillPop,
    this.flag,
  });

  String country;
  String countryAbbreviation;
  String totalCases;
  String newCases;
  String totalDeaths;
  String newDeaths;
  String totalRecovered;
  String activeCases;
  String seriousCritical;
  String casesPerMillPop;
  String flag;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    country: json["country"],
    countryAbbreviation: json["country_abbreviation"],
    totalCases: json["total_cases"],
    newCases: json["new_cases"],
    totalDeaths: json["total_deaths"],
    newDeaths: json["new_deaths"],
    totalRecovered: json["total_recovered"],
    activeCases: json["active_cases"],
    seriousCritical: json["serious_critical"],
    casesPerMillPop: json["cases_per_mill_pop"],
    flag: json["flag"],
  );

  Map<String, dynamic> toJson() => {
    "country": country,
    "country_abbreviation": countryAbbreviation,
    "total_cases": totalCases,
    "new_cases": newCases,
    "total_deaths": totalDeaths,
    "new_deaths": newDeaths,
    "total_recovered": totalRecovered,
    "active_cases": activeCases,
    "serious_critical": seriousCritical,
    "cases_per_mill_pop": casesPerMillPop,
    "flag": flag,
  };
}
