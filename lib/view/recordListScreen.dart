import 'package:covid_ui/common/ui_constants.dart';
import 'package:covid_ui/components/recordList.dart';
import 'package:covid_ui/model/Data.dart';
import 'package:flutter/material.dart';

class RecordListScreen extends StatefulWidget {
  final List<Data> data;
  RecordListScreen({this.data});
  @override
  _RecordListScreenState createState() => _RecordListScreenState();
}

class _RecordListScreenState extends State<RecordListScreen> {
  List<Data> data = [];
  @override
  void initState() {
    super.initState();
    data = widget.data;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: UIConstants.fitToHeight(62, context),
            width: UIConstants.fitToWidth(345, context),
            decoration: BoxDecoration(
                color: Color(0xff002097),
                borderRadius: BorderRadius.circular(5.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                displayWidget('Confirmed', '${data[0].totalCases}'),
                displayWidget('Recovered', '${data[0].totalRecovered}'),
                displayWidget('Deaths', '${data[0].totalDeaths}'),
              ],
            ),
          ),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Countries',
                  style: Theme.of(context).primaryTextTheme.caption.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600)),
              Container(
                width: UIConstants.fitToWidth(200, context),
                child: Padding(
                  padding: EdgeInsets.only(
                      right: UIConstants.fitToWidth(16, context)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total Cases',
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600)),
                      Text('Recovered',
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600)),
                      Text('Deaths',
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600)),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Flexible(
            child: ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                itemCount: data.length - 1,
                itemBuilder: (BuildContext context, int index) {
                  return RecordListCard(data: data[index + 1]);
                }),
          )
        ],
      ),
    );
  }

  Widget displayWidget(String title, String number) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('$title', style: Theme.of(context).primaryTextTheme.caption),
        Text('$number', style: Theme.of(context).primaryTextTheme.headline6)
      ],
    );
  }
}
