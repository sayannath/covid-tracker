import 'package:covid_ui/common/ui_constants.dart';
import 'package:covid_ui/model/Data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RecordListCard extends StatelessWidget {
  final Data data;
  RecordListCard({this.data});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: UIConstants.fitToHeight(8, context),
          bottom: UIConstants.fitToHeight(8, context)),
      child: Container(
        height: UIConstants.fitToHeight(40, context),
        width: UIConstants.fitToWidth(343, context),
        decoration: BoxDecoration(
            color: Color(0xffF0F1F3), borderRadius: BorderRadius.circular(5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: UIConstants.fitToWidth(8.0, context)),
                child: Image.network('${data.flag}', height: 20, width: 20),
              ),
              Text(
                '${data.country}',
                style: Theme.of(context).primaryTextTheme.caption.copyWith(
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600),
              ),
            ]),
            Container(
              width: UIConstants.fitToWidth(191, context),
              child: Padding(
                padding: EdgeInsets.only(
                    right: UIConstants.fitToWidth(8.0, context)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${data.totalCases}',
                      style: GoogleFonts.nunitoSans(
                          color: Color(0xff555770),
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.32),
                    ),
                    data.totalRecovered == "N/A"
                        ? Row(
                            children: [
                              Text('-'),
                              SizedBox(width: UIConstants.fitToWidth(4, context)),
                              Container(
                                height: UIConstants.fitToHeight(4, context),
                                width: UIConstants.fitToWidth(4, context),
                                decoration: BoxDecoration(
                                  color: Color(0xff6DA544),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              )
                            ],
                          )
                        : Row(
                            children: [
                              Text('${data.totalRecovered}',
                                  style: GoogleFonts.nunitoSans(
                                      color: Color(0xff555770),
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: -0.32)),
                              SizedBox(width: UIConstants.fitToWidth(4, context)),
                              Container(
                                height: UIConstants.fitToHeight(4, context),
                                width: UIConstants.fitToWidth(4, context),
                                decoration: BoxDecoration(
                                  color: Color(0xff6DA544),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              )
                            ],
                          ),
                    Text('${data.totalDeaths}',
                        style: GoogleFonts.nunitoSans(
                            color: Color(0xff555770),
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            fontStyle: FontStyle.normal,
                            letterSpacing: -0.32)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
