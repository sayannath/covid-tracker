import 'package:covid_ui/common/ui_constants.dart';
import 'package:flutter/material.dart';

class OptionCard extends StatelessWidget {
  Color _selectedColor = Color(0xff969696);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: UIConstants.fitToWidth(16, context),
          vertical: UIConstants.fitToHeight(16, context)),
      child: Container(
        height: UIConstants.fitToHeight(40, context),
        width: UIConstants.fitToWidth(343, context),
        decoration: BoxDecoration(
            color: Color(0xffFFFFFF), borderRadius: BorderRadius.circular(5.0)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
                onTap: () {

                },
                child: columnPart('List', Icons.list, context, _selectedColor)),
            InkWell(
                onTap: () {
                  _selectedColor = Color(0xffF40555);
                },
                child: columnPart(
                    'Map', Icons.location_on_outlined, context, _selectedColor))
          ],
        ),
      ),
    );
  }

  Widget columnPart(
      String title, IconData icon, BuildContext context, Color color) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(icon, color: color),
          SizedBox(width: 10.5),
          Text('$title',
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle1
                  .copyWith(color: Colors.black))
        ],
      ),
    );
  }
}
