import 'package:covid_ui/common/ui_constants.dart';
import 'package:covid_ui/model/Data.dart';
import 'package:covid_ui/services/covidService.dart';
import 'package:covid_ui/view/recordListScreen.dart';
import 'package:flutter/material.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  List<Data> data = [];
  bool isLoading = false;
  final scaffkey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    loadDataForScreen();
  }

  loadDataForScreen() async {
    setState(() {
      isLoading = true;
    });
    data = await CovidService.getData();
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffkey,
      body: Container(
        height: UIConstants.fitToHeight(812, context),
        width: UIConstants.fitToWidth(375, context),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              titleApp(context),
              switchApp(context),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: UIConstants.fitToWidth(16, context),
                    vertical: UIConstants.fitToHeight(16, context)),
                child: Container(
                  height: UIConstants.fitToHeight(40, context),
                  width: UIConstants.fitToWidth(343, context),
                  decoration: BoxDecoration(
                      color: Color(0xffFFFFFF),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      columnPart('List', Icons.list, context),
                      columnPart('Map', Icons.location_on_outlined, context)
                    ],
                  ),
                ),
              ),
              Container(
                  height: UIConstants.fitToHeight(607, context),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: Colors.white),
                  child: isLoading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : RecordListScreen(data: data))
            ],
          ),
        ),
      ),
    );
  }

  Widget titleApp(context) {
    return Padding(
      padding: EdgeInsets.only(
          left: UIConstants.fitToWidth(16, context),
          top: UIConstants.fitToWidth(44, context)),
      child:
          Text('Covid-19', style: Theme.of(context).primaryTextTheme.headline5),
    );
  }

  Widget switchApp(context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: UIConstants.fitToWidth(16, context),
          vertical: UIConstants.fitToHeight(8, context)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {},
                child: Container(
                  height: UIConstants.fitToHeight(40, context),
                  width: UIConstants.fitToWidth(109, context),
                  decoration: BoxDecoration(
                    color: Color(0xffF40555),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Center(
                    child: Text('Worldwide',
                        style: Theme.of(context).primaryTextTheme.subtitle1),
                  ),
                ),
              ),
              SizedBox(
                width: UIConstants.fitToWidth(8, context),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                    height: UIConstants.fitToHeight(40, context),
                    width: UIConstants.fitToWidth(109, context),
                    decoration: BoxDecoration(
                      color: Color(0xffF0F1F3),
                      border: Border.all(color: Color(0xffF40555)),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Center(
                        child: Text(
                      'Countries',
                      style: Theme.of(context)
                          .primaryTextTheme
                          .subtitle1
                          .copyWith(color: Color(0xff555770)),
                    ))),
              ),
            ],
          ),
          InkWell(
            onTap: () {},
            child: Icon(Icons.signal_cellular_alt_rounded,
                size: 38, color: Color(0xFF555770)),
          )
        ],
      ),
    );
  }

  Widget columnPart(String title, IconData icon, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(icon, color: Colors.red),
          SizedBox(width: 10.5),
          Text('$title',
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle1
                  .copyWith(color: Colors.black))
        ],
      ),
    );
  }
}
