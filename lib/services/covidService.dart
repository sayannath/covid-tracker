import 'dart:convert';

import 'package:covid_ui/model/Data.dart';
import 'package:http/http.dart' as http;

class CovidService {
  static Future<List<Data>> getData() async {
    String url =
        'https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search';
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      var responseMap = json.decode(response.body);
      List<Data> data =
          responseMap['data']['rows'].map<Data>((dataMap) => Data.fromJson(dataMap)).toList();
      return data;
    } else {
      print("DEBUG");
    }
  }
}
