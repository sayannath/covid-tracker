import 'package:covid_ui/view/splashScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Covid UI',
      home: SplashScreen(),
      theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: Color(0xffF0F1F3),
          primaryTextTheme: GoogleFonts.nunitoSansTextTheme(
                  Theme.of(context).textTheme)
              .copyWith(
                  headline6: GoogleFonts.nunitoSans(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.32),
                  headline5: GoogleFonts.nunitoSans(
                      color: Color(0xff002097),
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.32),
                  subtitle1: GoogleFonts.nunitoSans(
                      color: Color(0xffFFFFFF),
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.32),
                  caption: GoogleFonts.nunitoSans(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.32)),
          buttonTheme: ButtonThemeData(
              textTheme: ButtonTextTheme.primary,
              buttonColor: Color(0xffF40555),
              shape: RoundedRectangleBorder(
                  side: BorderSide(color: Color(0xffF40555)),
                  borderRadius: BorderRadius.circular(5.0)))),
    );
  }
}
